# BookAPI

.NET Core Web API

Intended for learning .NET Core Web API's as part of Module 3 of the .Net fullstack Noroff accelerate course.

Demonstrates the primary components of a functional Web API


## Getting Started

Clone to a local directory.

Open solution in Visual Studio

Run migrations

Run IIS Server

### Prerequisites

.NET Framework 5.0

Visual Studio 2017/19 OR Visual Studio Code


## Authors

***Dean von Schoultz** [deanvons](https://gitlab.com/deanvons)

Check List

[x] Book CRUD
[x] Book DTOs
[x] Publisher C
[x] Publisher R
[x] Publisher U
[x] Publisher D
[x] Publisher DTOS
[] Seed
[] Repository Pattern


