﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BookAPI.DTOs.Publisher;
using BookAPI.Models;

namespace BookAPI.Profiles
{
    public class PublisherProfile : Profile
    {
        public PublisherProfile()
        {
            CreateMap<Publisher, PublisherReadDTO>()
                .ForMember(prdto => prdto.Books,
                    opt => opt.MapFrom(b => b.Books
                        .Select(b => b.BookId).ToArray()));

            CreateMap<PublisherCreateDTO, Publisher>();
            CreateMap<PublisherUpdateDTO, Publisher>();
        }
    }
}
