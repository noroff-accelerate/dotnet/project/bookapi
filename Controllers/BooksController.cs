﻿using BookAPI.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BookAPI.DTOs.Book;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace BookAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BooksController : ControllerBase
    {
        private readonly BookDbContext _context;
        private readonly IMapper _mapper;

        public BooksController(BookDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        // GET: api/books
        /// <summary>
        /// Gets all books
        /// </summary>
        /// <returns>A Collection of Book objects</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<BookReadDTO>>> GetBooks()
        {
            var books = await _context.Books.ToListAsync();

            List<BookReadDTO> bookRead = _mapper.Map<List<BookReadDTO>>(books);

            return bookRead;
        }


        [HttpGet("{bookId}")]
        public async Task<ActionResult<BookReadDTO>> GetBookById(int bookId)
        {
            var book = await _context.Books.FindAsync(bookId);

            if (book == null)
            {
                return NotFound();
            }

            return _mapper.Map<BookReadDTO>(book);
        }

        // POST api/books
        /// <summary>
        /// Placeholder to add a book to the Books collection.
        /// </summary>
        /// <param name="book">book object from the body of the Post request (JSON)</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<BookReadDTO>> PostBook([FromBody] BookCreateDTO book)
        {
            Book domainBook = _mapper.Map<Book>(book);

            try
            {
                _context.Books.Add(domainBook);

                await _context.SaveChangesAsync();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            BookReadDTO newBook = _mapper.Map<BookReadDTO>(domainBook);

            return CreatedAtAction("GetBookById", new {bookId = newBook.BookId}, newBook);
        }

        /// <summary>
        /// Deletes a Book from the repository
        /// </summary>
        /// <param name="bookId">Id of the book which needs to be removed</param>
        /// <returns>No content</returns>
        [HttpDelete("{bookId}")]
        public async Task<ActionResult> Delete(int bookId)
        {
            var book = await _context.Books.FindAsync(bookId);

            if (book == null)
            {
                return NotFound();
            }

            _context.Books.Remove(book);
            await _context.SaveChangesAsync();

            return NoContent();
        }


        /// <summary>
        /// Updates a book.
        /// </summary>
        /// <param name="bookId">Id of the book which needs to be updated</param>
        /// <param name="updatedBook">The Book object which will replace the book with bookId</param>
        /// <returns>No Content</returns>
        [HttpPut("{bookId}")]
        public async Task<ActionResult> PutBook(int bookId, BookUpdateDTO updatedBook)
        {
            if (bookId != updatedBook.BookId)
            {
                return BadRequest();
            }

            Book domainBook = _mapper.Map<Book>(updatedBook);
            _context.Entry(domainBook).State = EntityState.Modified;


            await _context.SaveChangesAsync();


            return NoContent();
        }
    }
}