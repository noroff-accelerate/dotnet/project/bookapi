﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BookAPI.DTOs.Book;
using BookAPI.DTOs.Publisher;
using BookAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace BookAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PublishersController : ControllerBase
    {
        private readonly BookDbContext _context;
        private readonly IMapper _mapper;

        public PublishersController(BookDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all publishers
        /// </summary>
        /// <returns>List of publishers</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PublisherReadDTO>>> GetAll()
        {
            var domainPublishers = await _context.Publishers.Include(p => p.Books).ToListAsync();

            List<PublisherReadDTO> publishers = _mapper.Map<List<PublisherReadDTO>>(domainPublishers);

            return publishers;
        }

        /// <summary>
        /// Gets a single publisher using the publisherId
        /// </summary>
        /// <param name="publisherId">Id of the publisher</param>
        /// <returns>Publisher data or notfound</returns>
        [HttpGet("{publisherId}")]
        public async Task<ActionResult<PublisherReadDTO>> GetPublisherById(int publisherId)
        {
            var domainPublisher = await _context.Publishers.Include(p => p.Books)
                .FirstOrDefaultAsync(p => p.PublisherId == publisherId);

            if (domainPublisher == null)
            {
                return NotFound();
            }

            return _mapper.Map<PublisherReadDTO>(domainPublisher);
        }

        /// <summary>
        /// Adds a new publisher
        /// </summary>
        /// <param name="publisher">Publisher JSON object</param>
        /// <returns>Newly added publisher object</returns>
        [HttpPost]
        public async Task<ActionResult<PublisherReadDTO>> PostPublisher([FromBody] PublisherCreateDTO publisher)
        {
            var domainPublisher = _mapper.Map<Publisher>(publisher);

            try
            {
                _context.Publishers.Add(domainPublisher);

                await _context.SaveChangesAsync();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            PublisherReadDTO newPublisher = _mapper.Map<PublisherReadDTO>(domainPublisher);

            return CreatedAtAction("GetPublisherById", new {publisherId = newPublisher.PublisherId}, newPublisher);
        }

        [HttpDelete("{publisherId}")]
        public async Task<ActionResult> DeletePublisher(int publisherId)
        {
            var domainPublisher = await _context.Publishers.FindAsync(publisherId);

            if (domainPublisher == null)
            {
                return NotFound();
            }

            try
            {
                _context.Publishers.Remove(domainPublisher);

                await _context.SaveChangesAsync();
            }
            //Will likely get a referential integrity error when trying to delete a publisher (update this from badrequest to error message)
            catch (Exception e)
            {
                return BadRequest(e.GetType());
            }

            return NoContent();
        }


        /// <summary>
        /// Updates a publisher.
        /// </summary>
        /// <param name="publisherId">Id of the publisher which needs to be updated</param>
        /// <param name="updatedPublisher">The Publisher object which will replace the book with publisherId</param>
        /// <returns>No Content</returns>
        [HttpPut("{publisherId}")]
        public async Task<ActionResult> PutPublisher(int publisherId, PublisherUpdateDTO updatedPublisher)
        {
            if (publisherId != updatedPublisher.PublisherId)
            {
                return BadRequest();
            }

            Publisher domainPublisher = _mapper.Map<Publisher>(updatedPublisher);
            _context.Entry(domainPublisher).State = EntityState.Modified;


            await _context.SaveChangesAsync();


            return NoContent();
        }
    }
}
