﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookAPI.DTOs.Book
{
    public class BookReadDTO
    {
        public int BookId { get; set; }

        public string Title { get; set; }

        public string Genre { get; set; }

        public string Edition { get; set; }

        public double RetailPrice { get; set; }

        public string PublisherId { get; set; }
    }
}
