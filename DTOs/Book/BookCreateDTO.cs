﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BookAPI.DTOs.Book
{
    public class BookCreateDTO
    {
        [MaxLength(200)] public string Title { get; set; }

        [MaxLength(50, ErrorMessage = "That Genre is way too long")]
        public string Genre { get; set; }

        public string Edition { get; set; }

        public double CostPrice { get; set; }
        public double RetailPrice { get; set; }

        public int PublisherId { get; set; }
    }
}
