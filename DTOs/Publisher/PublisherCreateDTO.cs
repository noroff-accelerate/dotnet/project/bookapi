﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookAPI.DTOs.Publisher
{
    public class PublisherCreateDTO
    {
        public string Name { get; set; }

        public string OrganizationNumber { get; set; }
    }
}
