﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookAPI.DTOs.Publisher
{
    public class PublisherReadDTO
    {
        public int PublisherId { get; set; }

        public string Name { get; set; }

        public List<int> Books { get; set; }
    }
}
