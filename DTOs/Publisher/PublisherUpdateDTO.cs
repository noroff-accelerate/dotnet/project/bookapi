﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookAPI.DTOs.Publisher
{
    public class PublisherUpdateDTO
    {
        public int PublisherId { get; set; }

        public string Name { get; set; }

        public string OrganizationNumber { get; set; }

        public double GrossRevenue { get; set; }
    }
}
