﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Threading.Tasks;

namespace BookAPI.Models
{
    public class Publisher
    {
        public int PublisherId { get; set; }

        public string Name { get; set; }

        public string OrganizationNumber { get; set; }

        public double GrossRevenue { get; set; }

        public ICollection<Book> Books { get; set; }
    }
}
