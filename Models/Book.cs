﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BookAPI.Models
{
    public class Book
    {
        public int BookId { get; set; }

        [MaxLength(13)] public string ISBN10 { get; set; }

        [MaxLength(17)] public string ISBN13 { get; set; }

        [MaxLength(200)] public string Title { get; set; }

        public string Genre { get; set; }

        public string Edition { get; set; }

        public double CostPrice { get; set; }
        public double RetailPrice { get; set; }

        public int PublisherId { get; set; }
        public Publisher Publisher { get; set; }
    }
}
